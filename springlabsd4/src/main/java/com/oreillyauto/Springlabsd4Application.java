package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springlabsd4Application {

	public static void main(String[] args) {
		SpringApplication.run(Springlabsd4Application.class, args);
	}

}
