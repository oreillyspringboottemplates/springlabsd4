package com.oreillyauto.service;

import com.oreillyauto.domain.Carpart;

public interface CarpartsService {
	
	Carpart getCarpartByPartNumber(String partNumber);
	Carpart getCarpart(String partNumber) throws Exception;
}
