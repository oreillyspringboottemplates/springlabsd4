<%@ include file="/WEB-INF/layouts/include.jsp" %>
<h1>Oh Really</h1>
<h4 class="mb-4">Transaction Report</h4>
<div class="row">
	<div class="col-6">
		<h6 id="totalTransactions" class="d-inline-block mr-2">Total Transactions:</h6><span>${totalTransactions}</span>
	</div>
	<div class="col-6">
		<h6 class="d-inline-block mr-2">Report Date:</h6><span id="reportDate"></span>
	<div>
</div>
<script>
	orly.ready.then(() => {
		setReportDate();
	});
	
	function setReportDate() {
		let reportDate = orly.qid("reportDate");
		reportDate.innerHTML = new Date();
	}
</script>